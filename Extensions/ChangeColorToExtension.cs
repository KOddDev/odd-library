﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using OddStatue.Tools;

namespace OddStatue.Extensions
{
    
    public static class ChangeColorToExtension
    {
        private static CallBack myCallbacks = new CallBack();
        private static ExtensionMethodHelper myHelper = ExtensionMethodHelper.Instance;

        public static void ChangeColorTo(this Image _img, Color _newColor, float _time)
        {
            ImgClrChanger clrChanger = new ImgClrChanger(_img);
            myHelper.CoroutineStart(clrChanger.transform, ChangingColor(clrChanger, _newColor, _time));
        }
        public static void ChangeColorTo(this Image _img, Color _newColor, float _time, CallBack.CallbackDelegate _callBack)
        {
            _img.ChangeColorTo(_newColor, _time);
            myCallbacks.AddCallback(_img.transform, _callBack);
        }

        public static void ChangeColorTo(this Material _mat, Color _newColor, float _time)
        {
            //Den mporw na vrw to transgorm apo to material edw.
            myHelper.StartCoroutine(ChangingColor(new MatClrChanger(_mat), _newColor, _time));
        }
        public static void ChangeColorTo(this Material _mat, Color _newColor, float _time, Transform _transform, CallBack.CallbackDelegate _callBack)
        {
            MatClrChanger clrChanger = new MatClrChanger(_mat, _transform);
            myHelper.CoroutineStart(clrChanger.transform,ChangingColor(clrChanger, _newColor, _time));
            myCallbacks.AddCallback(_transform, _callBack);
        }

        public static void ChangeColorTo(this Material _mat, string _name, Color _newColor, float _time)
        {
            //Den mporw na vrw to transgorm apo to material edw.
            myHelper.StartCoroutine(ChangingColor(new ShaderMatChanger(_name, _mat), _newColor, _time));
        }
        public static void ChangeColorTo(this Material _mat, string _name, Color _newColor, float _time, Transform _transform, CallBack.CallbackDelegate _callBack)
        {
            ShaderMatChanger clrChanger = new ShaderMatChanger(_name, _mat, _transform);
            myHelper.CoroutineStart(clrChanger.transform,ChangingColor(new ShaderMatChanger(_name, _mat, _transform), _newColor, _time));
            myCallbacks.AddCallback(_transform, _callBack);
        }

        public static void ChangeColorTo(this Text _txt, Color _newColor, float _time)
        {
            TxtClrChanger clrChanger = new TxtClrChanger(_txt);
            myHelper.CoroutineStart(clrChanger.transform,ChangingColor(clrChanger, _newColor, _time));
        }
        public static void ChangeColorTo(this Text _txt, Color _newColor, float _time, CallBack.CallbackDelegate _callBack)
        {
            _txt.ChangeColorTo(_newColor, _time);
            myCallbacks.AddCallback(_txt.transform, _callBack);
        }

        private static IEnumerator ChangingColor(ColorChanger _clrHolder, Color _newColor, float _timeToMove)
        {
            float t = 0f;
            Color currColor = _clrHolder.Color;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _clrHolder.Color = Color.Lerp(currColor, _newColor, t);
                yield return null;
            }
            //CallBalck function        
            myCallbacks.RunCallBack(_clrHolder.transform);
            myHelper.CoroutineStop(_clrHolder.transform, ChangingColor(_clrHolder, _newColor, _timeToMove));
        }

        #region Help classes for ChangingColor
        abstract class ColorChanger
        {
            public Transform transform;
            public virtual Color Color
            {
                get { return Color.black; }
                set { }
            }
        }
        class ImgClrChanger : ColorChanger
        {
            public Image img;
            public override Color Color
            {
                get { return img.color; }
                set { img.color = value; }
            }
            public ImgClrChanger(Image _img) { img = _img; transform = _img.transform; }
        }
        class MatClrChanger : ColorChanger
        {
            public Material mat;
            //public string name;
            public override Color Color
            {
                get { return mat.color; }
                set { mat.color = value; }
            }
            public MatClrChanger(Material _mat) { mat = _mat;}
            public MatClrChanger(Material _mat, Transform _transform) { mat = _mat; transform = _transform; }
        }
        class ShaderMatChanger : ColorChanger
        {
            public Material mat;
            public string name;
            public override Color Color
            {
                get { return mat.GetColor(name); }
                set { mat.SetColor(name, value); }
            }
            public ShaderMatChanger(string _name, Material _mat) { name = _name; mat = _mat; }
            public ShaderMatChanger(string _name, Material _mat, Transform _transform) { name = _name; mat = _mat; transform = _transform; }
        }
        class TxtClrChanger : ColorChanger
        {
            public Text txt;
            public override Color Color
            {
                get { return txt.color; }
                set { txt.color = value; }
            }
            public TxtClrChanger(Text _txt) { txt = _txt; transform = _txt.transform; }
        }
        #endregion
    }
}