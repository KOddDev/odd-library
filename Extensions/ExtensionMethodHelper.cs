﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OddStatue.Extensions
{
    public class ExtensionMethodHelper : MonoBehaviour
    {
        private static ExtensionMethodHelper _Instance;        
        public static ExtensionMethodHelper Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GameObject("ExtensionMethodHelper").AddComponent<ExtensionMethodHelper>();
                    activeCoroutines = new Dictionary<int, List<IEnumerator>>();
                }
                return _Instance;
            }
        }

        private static Dictionary<int, List<IEnumerator>> activeCoroutines;

        public void CoroutineStart(Transform _transform, IEnumerator _coroutine)
        {
            int id = _transform.GetInstanceID();
            if (!activeCoroutines.ContainsKey(id))
            {
                List<IEnumerator> coroutineList = new List<IEnumerator>();
                activeCoroutines.Add(id, coroutineList);
            }
            activeCoroutines[id].Add(_coroutine);

            Instance.StartCoroutine(_coroutine);
        }

        public void CoroutineStop(Transform _transform, IEnumerator _coroutine)
        {
            int id = _transform.GetInstanceID();
            if (activeCoroutines.ContainsKey(id) && activeCoroutines[id].Contains(_coroutine))
            {
                activeCoroutines[id].Remove(_coroutine);
                if (activeCoroutines[id].Count == 0)
                    activeCoroutines.Remove(id);
            }
            Instance.StopCoroutine(_coroutine);
        }

        public void CoroutineStop(Transform _transform)
        {
            int id = _transform.GetInstanceID();
            if (activeCoroutines.ContainsKey(id))
            {
                int length = activeCoroutines[id].Count;
                for (int i = 0; i < length; i++)
                    Instance.StopCoroutine(activeCoroutines[id][i]);
                activeCoroutines.Remove(id);
            }
        }

    }
}