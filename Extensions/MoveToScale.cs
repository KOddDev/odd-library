﻿using System.Collections;
using UnityEngine;
using OddStatue.Tools;

namespace OddStatue.Extensions
{
    public static class MoveToScale
    {
        private static CallBack myCallbacks = new CallBack();
        private static ExtensionMethodHelper myHelper = ExtensionMethodHelper.Instance;

        public static void MoveToSca(this Transform _obj, Vector3 _scale, float _time)
        {
            Scale(_obj, _scale, _time, null);
        }
        public static void MoveToSca(this Transform _obj, Vector3 _scale, float _time, CallBack.CallbackDelegate _callback)
        {
            Scale(_obj, _scale, _time, _callback);
        }
        private static void Scale(Transform _obj, Vector3 _scale, float _time, CallBack.CallbackDelegate _callback)
        {
            if (_callback != null)
                myCallbacks.AddCallback(_obj, _callback);

            myHelper.CoroutineStart(_obj,Scaling(_obj, _scale, _time));
        }

        static private IEnumerator Scaling(Transform _transform, Vector3 _scale, float _timeToMove)
        {
            Vector3 currentScale = _transform.localScale;
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _transform.localScale = Vector3.Lerp(currentScale, _scale, t);
                yield return null;
            }
            int id = _transform.GetInstanceID();
            myCallbacks.RunCallBack(_transform);
            myHelper.CoroutineStop(_transform, Scaling(_transform, _scale, _timeToMove));
        }
    }
}
