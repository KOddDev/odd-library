﻿namespace OddStatue.Extensions
{
    public static class InRange
    {
        public static bool IsInRange(this float _num, float _range) => _num.IsInRange(_num - _range, _num + _range);

        public static bool IsInRange(this int _num, int _range)=> _num.IsInRange(_num - _range, _num + _range);

        public static bool IsInRange(this float _num, float _min, float _max) => _num > _min && _num<_max;

        public static bool IsInRange(this int _num, int _min, int _max) => _num > _min && _num < _max;
    }
}

