﻿using UnityEngine;
using System.Collections.Generic;

namespace OddStatue.Extensions
{
    public static class ArrayExtension
    {
        public static void Shuffle<T>(this T[] _array)
        {
            int length = _array.Length;
            for (int i = 0; i < length; i++)
            {
                T temp = _array[i];
                int randomIndex = Random.Range(i, length);
                _array[i] = _array[randomIndex];
                _array[randomIndex] = temp;
            }
        }

        public static List<T> ToList<T>(this T[] _array)
        {
            List<T> list = new List<T>();
            int length = _array.Length;
            for (int i = 0; i < length; i++)
                list.Add(_array[i]);
            return list;
        }
    }
}