﻿using System.Collections;
using UnityEngine;
using OddStatue.Tools;

namespace OddStatue.Extensions
{
    public static class MoveToPosition
    {
        private static CallBack myCallbacks = new CallBack();
        private static ExtensionMethodHelper myHelper = ExtensionMethodHelper.Instance;

        public static void MoveToPos(this Transform _obj, Vector3 _pos, float _time)
        {
            Move(_obj, _pos, _time, false, null);
        }
        public static void MoveToPos(this Transform _obj, Vector3 _pos, float _time, bool _local)
        {
            Move(_obj, _pos, _time, _local, null);
        }
        public static void MoveToPos(this Transform _obj, Vector3 _pos, float _time, bool _local, CallBack.CallbackDelegate _callback)
        {
            Move(_obj, _pos, _time, _local, _callback);
        }
        private static void Move(Transform _obj, Vector3 _pos, float _time, bool _local, CallBack.CallbackDelegate _callback)
        {
            if (_callback != null)
                myCallbacks.AddCallback(_obj, _callback);
            if (_local)
                myHelper.CoroutineStart(_obj, MoveToLocalPosition(_obj, _pos, _time));
            else
                myHelper.CoroutineStart(_obj, MoveToWorldPosition(_obj, _pos, _time));
        }

        static private IEnumerator MoveToWorldPosition(Transform _transform, Vector3 _position, float _timeToMove)
        {
            Vector3 currentPos = _transform.position;
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _transform.position = Vector3.Lerp(currentPos, _position, t);
                yield return null;
            }
            myCallbacks.RunCallBack(_transform);
            myHelper.CoroutineStop(_transform, MoveToLocalPosition(_transform, _position, _timeToMove));
        }
        static private IEnumerator MoveToLocalPosition(Transform _transform, Vector3 _position, float _timeToMove)
        {
            Vector3 currentPos = _transform.localPosition;
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _transform.localPosition = Vector3.Lerp(currentPos, _position, t);
                yield return null;
            }
            myCallbacks.RunCallBack(_transform);
            myHelper.CoroutineStop(_transform, MoveToLocalPosition(_transform, _position, _timeToMove));
        }

    }
}