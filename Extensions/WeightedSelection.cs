﻿using System.Collections.Generic;
using System;

#region Implementation Example
//using System.Collections.Generic;
//using UnityEngine;
//using OddStatue.Extensions.WeightedSelection;

//public class Test : MonoBehaviour
//    {
//        void Start()
//        {
//            List<TestClassWithWeight> tesObjects = new List<TestClassWithWeight>();
//            tesObjects.Add(new TestClassWithWeight("a", 5));
//            tesObjects.Add(new TestClassWithWeight("b", 15));
//            tesObjects.Add(new TestClassWithWeight("c", 5));

//            Debug.Log(tesObjects.RandomElementByWeight().name);
//        }
//    }

//public class TestClassWithWeight : IWeightedObject
//{
//    public float SelectionWeight { get { return weight; } }
//    private float weight { get; set; }
//    public string name;

//    public TestClassWithWeight(string _name, float _weight)
//    {
//        weight = _weight;
//        name = _name;
//    }
//}
#endregion

namespace OddStatue.Extensions.WeightedSelection
{

    /// <summary>
    /// Interface that must be implemented in order to have Weighted Selection or Roulette wheel selection working
    /// </summary>
    public interface IWeightedObject
    {
        float SelectionWeight { get; }
    }

    public static class WeightedSelection
    {
        /// <summary>
        /// Weighted Selection or Roulette wheel selection
        /// </summary>
        /// <typeparam name="T">IWeightedObject interface</typeparam>
        /// <param name="_list">list of IWeightedObjects</param>
        /// <returns>Returns a random element of a weighted list</returns>
        public static T RandomElementByWeight<T>(this IReadOnlyList<T> _list) where T : IWeightedObject
        {
            float totalWeight = _list.TotalWeight();
            return _list.SelectElement((float)new Random().NextDouble() * totalWeight);
        }
       
        /// <summary>
        /// Weighted Selection or Roulette wheel selection
        /// </summary>
        /// <typeparam name="T">IWeightedObject interface</typeparam>
        /// <param name="_list">list of IWeightedObjects</param>
        /// <param name="_population">the lenght of return array</param>
        /// <returns>Returns an array of random elements of a weighted list</returns>
        public static T[] RandomElementsByWeight<T>(this IReadOnlyList<T> _list, int _population) where T : IWeightedObject
        {
            T[] returnArray = new T[_population];
            float totalWeight = _list.TotalWeight();
            for (int i = 0; i < _population; i++)
                returnArray[i]= _list.SelectElement((float)new Random().NextDouble() * totalWeight);
            return returnArray;
        }

        /// <summary>
        /// Total weight of weighted list
        /// </summary>
        /// <typeparam name="T">IWeightedObject interface</typeparam>
        /// <param name="_list">list of IWeightedObjects</param>
        /// <returns>returns the total weight of weighted list</returns>
        public static float TotalWeight<T>(this IReadOnlyList<T> _list) where T : IWeightedObject
        {
            float weight = 0;
            int length = _list.Count;
            for (int i = 0; i < length; i++)
                weight += _list[i].SelectionWeight;
            return weight;
        }

        /// <summary>
        /// Returns the item of the list or array with the chosen weight
        /// </summary>
        /// <typeparam name="T">IWeightedObject interface</typeparam>
        /// <param name="_list">list of IWeightedObjects</param>
        /// <param name="_itemWeightIndex">The weight we are after...</param>
        /// <returns></returns>
        private static T SelectElement<T>(this IReadOnlyList<T> _list, float _itemWeightIndex) where T : IWeightedObject
        {
            float currentWeightIndex = 0;
            int length = _list.Count;
            for (int i = 0; i < length; i++)
            {
                currentWeightIndex += _list[i].SelectionWeight;
                // If we've hit or passed the weight we are after for this item then it's the one we want....
                if (currentWeightIndex >= _itemWeightIndex)
                    return _list[i];
            }
            return default;
        }
    }
}
