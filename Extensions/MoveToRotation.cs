﻿using System.Collections;
using UnityEngine;
using OddStatue.Tools;

namespace OddStatue.Extensions
{
    public static class MoveToRotation
    {
        private static CallBack myCallbacks = new CallBack();
        private static ExtensionMethodHelper myHelper = ExtensionMethodHelper.Instance;
        
        public static void MoveToRot(this Transform _obj, Vector3 _rot, float _time)
        {
            Rotate(_obj, _rot, _time, false, null);
        }
        public static void MoveToRot(this Transform _obj, Vector3 _rot, float _time, bool _local)
        {
            Rotate(_obj, _rot, _time, _local, null);
        }
        public static void MoveToRot(this Transform _obj, Vector3 _rot, float _time, bool _local, CallBack.CallbackDelegate _callback)
        {
            Rotate(_obj, _rot, _time, _local, _callback);
        }
        private static void Rotate(Transform _obj, Vector3 _rot, float _time, bool _local, CallBack.CallbackDelegate _callback)
        {
            if (_callback != null)
                myCallbacks.AddCallback(_obj, _callback);
            if (_local)
                myHelper.CoroutineStart(_obj,MoveToLocalRotation(_obj, _rot, _time));
            else
                myHelper.CoroutineStart(_obj,MoveToWorldRotation(_obj, _rot, _time));
        }

        static private IEnumerator MoveToWorldRotation(Transform _transform, Vector3 _rotation, float _timeToMove)
        {
            Quaternion currentRotation = _transform.rotation;
            Quaternion newRot = Quaternion.Euler(_rotation);
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _transform.rotation = Quaternion.Lerp(currentRotation, newRot, t);
                yield return null;
            }
            myCallbacks.RunCallBack(_transform);
            myHelper.CoroutineStop(_transform, MoveToWorldRotation(_transform, _rotation, _timeToMove));
        }
        static private IEnumerator MoveToLocalRotation(Transform _transform, Vector3 _rotation, float _timeToMove)
        {
            Quaternion currentRotation = _transform.rotation;
            Quaternion newRot = Quaternion.Euler(_rotation);
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / _timeToMove;
                _transform.localRotation = Quaternion.Lerp(currentRotation, newRot, t);
                yield return null;
            }
            myCallbacks.RunCallBack(_transform);
            myHelper.CoroutineStop(_transform, MoveToLocalRotation(_transform, _rotation, _timeToMove));
        }
    }
}
