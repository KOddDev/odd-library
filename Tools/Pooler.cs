﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class allows us to create multiple instances of a prefabs and reuse them.
/// It allows us to avoid the cost of destroying and creating objects.
/// </summar>
namespace OddStatue.Tools
{
    public class Pooler
     {
        protected Stack<GameObject> freeInstances = new Stack<GameObject>();
        protected GameObject original;
        protected Transform originalParent;

        public Pooler(GameObject _original, int _initialSize): this(_original, _initialSize, null) { }
        public Pooler(GameObject _original, int _initialSize,Transform _parent)
        {
            this.original = _original;
            this.originalParent = _parent;
            freeInstances = new Stack<GameObject>(_initialSize);

            for (int i = 0; i < _initialSize; ++i)
            {
                GameObject obj = Object.Instantiate(_original, _parent);
                obj.SetActive(false);
                freeInstances.Push(obj);
            }
        }

        public GameObject Get(){ return Get(Vector3.zero, Quaternion.identity); }
        public GameObject Get(Vector3 _pos) { return Get(_pos, Quaternion.identity); }
        public GameObject Get(Vector3 _pos, Quaternion _quat)
        {
            GameObject ret = freeInstances.Count > 0 ? freeInstances.Pop() : Object.Instantiate(original,originalParent);

            ret.SetActive(true);
            ret.transform.position = _pos;
            ret.transform.rotation = _quat;

            return ret;
        }

        public void Free(GameObject _obj)
        {
            _obj.SetActive(false);
            freeInstances.Push(_obj);
        }
    }
}