﻿using System.Collections.Generic;
using UnityEngine;

namespace OddStatue.Tools
{
    public class CallBack
    {
        public delegate void CallbackDelegate();
        static private Dictionary<int, CallbackDelegate> callbacks = new Dictionary<int, CallbackDelegate>();

        public static void RemoveCallBack(Transform _transform)
        {
            int id = _transform.GetInstanceID();
            if (callbacks.ContainsKey(id))
                callbacks.Remove(id);
        }

        public void RunCallBack(Transform _transform)
        {
            int id = _transform.GetInstanceID();
            if (callbacks.ContainsKey(id))
            {
                CallbackDelegate tempCallback = callbacks[id];
                callbacks.Remove(id);
                tempCallback();
            }
        }

        public void AddCallback(Transform _transform,CallbackDelegate _callback)
        {
            int id = _transform.GetInstanceID();
            if (callbacks.ContainsKey(id))
                return;
            callbacks.Add(id, _callback);
        }
    }
}
