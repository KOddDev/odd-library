﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace OddStatue.Tools
{
    public static class MouseTouchInput
    {
        private static EventSystem currEventSys;

        public static void Init() => currEventSys = EventSystem.current;
        public static void Init(EventSystem _curr) => currEventSys = _curr;

#if UNITY_ANDROID || UNITY_IOS
    public static bool InputControlDown()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            return true;
        return false;
    }

    public static bool InputControlUp()
    {
        if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled))
            return true;
        return false;
    }
    public static bool InputControl()
    {
        if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary))
            return true;
        return false;
    }

    public static bool IsPointerOverUIObject()
    {
        if (Input.touchCount == 0)
            return false;
        PointerEventData eventDataCurrentPosition = new PointerEventData(currEventSys);
        eventDataCurrentPosition.position = new Vector2(InputPosition().x, InputPosition().y);
        List<RaycastResult> results = new List<RaycastResult>();
        currEventSys.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    public static Vector3 InputPosition()
    {
        return Input.GetTouch(0).position;
    }
#else
        public static bool InputControlDown()
        {
            return Input.GetMouseButtonDown(0);
        }
        public static bool InputControlUp()
        {
            if (Input.GetMouseButtonUp(0))
                return true;
            return false;
        }
        public static bool InputControl()
        {
            if (Input.GetMouseButton(0))
                return true;
            return false;
        }
        public static bool IsPointerOverUIObject()
        {
            return currEventSys.IsPointerOverGameObject();
        }
        public static Vector3 InputPosition()
        {
            return Input.mousePosition;
        }
#endif

        public static bool InputControl(bool _checkOverUI)
        {
            return _checkOverUI ? (IsPointerOverUIObject() ? false : InputControl()) : InputControl();
        }
        public static bool InputControlDown(bool _checkOverUI)
        {
            return _checkOverUI ? (IsPointerOverUIObject() ? false : InputControlDown()) : InputControlDown();
        }
        public static bool InputControlUp(bool _checkOverUI)
        {
            return _checkOverUI ? (IsPointerOverUIObject() ? false : InputControlUp()) : InputControlUp();
        }
        public static bool InputPosition(out Vector2 _pos)
        {
            if (InputControl())
            {
                _pos = InputPosition();
                return true;
            }
            _pos = Vector2.zero;
            return false;
        }
    }
}
