﻿using System.Collections;
using UnityEngine;
using UnityEngine.Monetization;
using OddStatue.UnityAds;

public class MonetazationManager : MonoBehaviour
{
    public IAwardPrize reward;

    void Start()
    {
        MonetizationInfo.Instance.manager = this;
#if UNITY_ANDROID
        Monetization.Initialize(AdConsts.gameIdAndroid, AdConsts.testMode);
#elif UNITY_IOS
         Monetization.Initialize(AdConsts.gameIdIos, AdConsts.testMode);
#endif
        StartCoroutine(WaitForInitialization());

    }
    private IEnumerator WaitForInitialization()
    {
        do
        {
            yield return new WaitForSeconds(0.1f);
        } while (!Monetization.isInitialized);
        StartCoroutine(WaitForAdLoad(AdConsts.rewardedVideo));
    }

    private IEnumerator WaitForAdLoad(string _id)
    {
        while (!Monetization.IsReady(_id))
        {
            yield return new WaitForSeconds(0.1f);
        }
        Ready2ShowAd(_id);
    }

    /// <summary>
    /// Logic of what happens when an add is downloaded (ex. show it, show a button etc)
    /// </summary>
    public virtual void Ready2ShowAd(string _id)
    {
        ShowAd(_id);
    }

    public void ShowAd(string _id)
    {
        if (!Monetization.IsReady(_id))
        {
            WaitForAdLoad(_id);
            return;
        }

        ShowAdPlacementContent ad = null;
        ad = Monetization.GetPlacementContent(_id) as ShowAdPlacementContent;

        if (ad != null)
        {
            ad.Show(RewardedAdFinished);
        }
    }

    void RewardedAdFinished(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            // Reward the player
            reward.GivePrize();
            StartCoroutine(WaitForAdLoad(AdConsts.rewardedVideo));
        }
        reward.VideoEnded();
    }
}