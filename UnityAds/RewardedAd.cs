﻿using UnityEngine;
using OddStatue.UnityAds;

public class RewardedAd : MonoBehaviour,IAwardPrize
{
    void Start()
    {
        MonetizationInfo.Instance.manager.reward = this;
    }
    /// <summary>
    /// what the player will win
    /// </summary>
    public void GivePrize()
    {
        
    }

    /// <summary>
    /// what must be done after an add is shown
    /// </summary>
    public void VideoEnded()
    {

    }
}
