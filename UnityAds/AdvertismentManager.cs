﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using OddStatue.UnityAds;


public class AdvertismentManager : MonoBehaviour
{
    void Start()
    {
#if UNITY_IOS
        Advertisement.Initialize(AdConsts.gameIdIos,AdConsts.testMode);
#elif UNITY_ANDROID
        Advertisement.Initialize(AdConsts.gameIdAndroid,AdConsts.testMode);
#endif
        StartCoroutine(ShowBannerWhenReady(AdConsts.bannerPlacement));
    }

    IEnumerator ShowBannerWhenReady(string _id)
    {
        while (!Advertisement.IsReady(_id))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(_id);
    }
}

