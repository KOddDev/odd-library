﻿namespace OddStatue.UnityAds
{
    public static class AdConsts
    {
        internal static string gameIdAndroid = "3089481";
        internal static string gameIdIos = "";
        internal static bool testMode = true;
        internal static string rewardedVideo = "rewardedVideo";
        internal static string bannerPlacement = "BannerAd";
    }
    
    public interface IAwardPrize
    {
        void GivePrize();
        void VideoEnded();
    }
    
    public class MonetizationInfo
    {
        private static MonetizationInfo instance;
        public static MonetizationInfo Instance
        {
            get
            {
                if (instance == null)
                    instance = new MonetizationInfo();
                return instance;
            }
        }
        public MonetazationManager manager;
    }
}