﻿using UnityEngine;

public class ScreenShotHandler : MonoBehaviour {
    public int width, height;

    private static ScreenShotHandler instance;
    private Camera myCamera;
    private bool takeScreenshot;

    void Awake()
    {
        instance = this;
        myCamera = gameObject.GetComponent<Camera>();
    }
    void OnPostRender()
    {
        if (takeScreenshot)
        {
            takeScreenshot = false;
            RenderTexture renderTexture = myCamera.targetTexture;

            Texture2D renderResult = new Texture2D(width, height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, width, height);
            renderResult.ReadPixels(rect, 0, 0);


            byte[] byteArray = renderResult.EncodeToPNG();
            System.IO.File.WriteAllBytes(Application.dataPath + "/CameraScreenShot.png", byteArray);

            RenderTexture.ReleaseTemporary(renderTexture);
            myCamera.targetTexture = null;

        }
    }

    public void TakeScreenshot()
    {
        myCamera.targetTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 16);
        takeScreenshot = true;
    }

}
